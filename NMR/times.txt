	num_iter_in = 50;
	num_iter_out = 1;
	delta_t = 1/7;
	kappa = 0.002;
	option = 2;

	iters		time[s]
	50			0.337336 s
	100			0.512781 s
	200			0.871365 s
	400			1.603641 s